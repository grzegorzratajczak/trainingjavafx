package bookJavaFX9.chapter01;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * example of life cycle in JavaFX application
 * main() -> Application.launch() -> init() -> start() -> stop() -back to> main()
 */
public class Listing01_5 extends Application {

    public static void main(String[] args) {
        System.out.println("Launch main() method");
        Application.launch(args);
        System.out.println("Back to main() method");
    }

    @Override
    public void init(){
        System.out.println("Launch init() method");
    }

    public void start(Stage primaryStage) throws Exception {
        System.out.println("Launch start() method");
        StackPane root = new StackPane();
        Scene scene = new Scene(root, 300, 200);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void stop(){
        System.out.println("Launch stop() method");
    }
}
