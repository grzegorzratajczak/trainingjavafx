package bookJavaFX9.chapter01;

import javafx.application.Application;
import javafx.stage.Stage;

public class Listing01_3 extends Application {

    public static void main(String[] args) {
        Application.launch(args);   //should be only method here, add for some IDE
    }

    @Override
    public void init(){}    //method executed before start(), could be use example for DB connection, etc.

    @Override
    public void start(Stage primaryStage) throws Exception {}

    @Override
    public void stop(){}    //method executed after start(), example for close connection, save data, etc.

}
