package bookJavaFX9.chapter02;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

/**
 * example JavaBean with listener
 */
public class Listing02_02 implements Serializable {

    private static final long serialVersionUID = -2464305726678039614L;
    private int calNumber;
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    public Listing02_02() {
        this(0);
    }

    public Listing02_02(int calNumber){
        this.calNumber = calNumber;
    }

    public int getCalNumber(){
        return calNumber;
    }

    public synchronized void addPropertyChangeListener(PropertyChangeListener list1){
        pcs.addPropertyChangeListener(list1);
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener list2){
        pcs.removePropertyChangeListener(list2);
    }

    public synchronized void setCalNumber(int newCalNumber){
        int oldValue = calNumber;
        calNumber = newCalNumber;
        pcs.firePropertyChange("Change of calNumber", Integer.valueOf(oldValue), Integer.valueOf(newCalNumber));
    }

}
