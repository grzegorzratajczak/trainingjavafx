package bookJavaFX9.chapter03;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

/**
 * Create observable list with listener
 */

public class Listing03_02 {

    public static void main(String[] args) {

        ObservableList<Integer> observableList = FXCollections.observableArrayList();
        observableList.addListener((ListChangeListener<Integer>) c -> {
            while (c.next()) {
                if (c.wasRemoved()) {
                    System.out.println("Delete: " + c.getRemoved());
                }
            }
        });
        observableList.add(1);
        observableList.addAll(2, 4, 6, 8, 10, 12, 14, 16, 18, 20);
        observableList.remove(4);   // Index!!!
        observableList.remove(Integer.valueOf(4));  // Integer value
        System.out.println(observableList);
    }
}
