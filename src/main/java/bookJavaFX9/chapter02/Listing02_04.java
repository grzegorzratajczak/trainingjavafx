package bookJavaFX9.chapter02;

import java.util.Arrays;

/**
 * JavaBean with index property
 */
public class Listing02_04 {

    private int[] array;

    public Listing02_04() {
        this(0);
    }

    public Listing02_04(int length) {
        array = new int[length];
    }

    public int[] getArray() {
        return Arrays.copyOf(array, array.length);
    }

    public int getArray(int index) {
        return array[index];
    }

    public void setArray(int[] arr) {
        this.array = Arrays.copyOf(arr, arr.length);
    }

    public void setArray(int index, int value) {
        array[index] = value;
    }
}
