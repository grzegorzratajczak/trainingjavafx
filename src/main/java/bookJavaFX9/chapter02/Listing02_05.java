package bookJavaFX9.chapter02;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * Property R/W
 */
public class Listing02_05 {

    public static void main(String[] args) {

        IntegerProperty age = new SimpleIntegerProperty();
        age.set(33);
        System.out.println(age.get());
    }
}
