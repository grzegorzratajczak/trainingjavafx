package bookJavaFX9.chapter02;

/**
 * example of JavaFXBean with Read only property
 */
public class Listing02_08Main {

    public static void main(String[] args) {

        Listing02_08 pesel = new Listing02_08("Pablo", 352524);
        //one way to get property value
        System.out.println(pesel.nickProperty().get());
        System.out.println(pesel.peselProperty().get());
        //second way to get property value
        System.out.println(pesel.getNick() + ", " + pesel.getPesel());

    }
}
