package bookJavaFX9.chapter02;

import java.io.Serializable;

/**
 * example JavaBean class
 */

public class Listing02_01 implements Serializable {

    private static final long serialVersionUID = 3106122450520221444L;
    private int calNumber;

    public Listing02_01(){
        this(0);
    }

    public Listing02_01(int calNumber){
        this.calNumber = calNumber;
    }

    public int getCalNumber(){
        return calNumber;
    }

    public void setCalNumber(int calNumber){
        this.calNumber = calNumber;
    }
}
