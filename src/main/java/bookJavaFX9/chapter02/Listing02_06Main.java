package bookJavaFX9.chapter02;

/**
 * example of using javafxbean property
 */
public class Listing02_06Main {

    public static void main(String[] args) {
        Listing02_06 pesel = new Listing02_06();
        pesel.nickProperty().set("Pablo");
        pesel.peselProperty().set(23523);
        //one way to take property value
        String nick = pesel.getNick();
        int nr = pesel.getPesel();
        System.out.println(nick + ", " + nr);
        //second way to take property value
        String nick1 = pesel.nickProperty().get();
        int nr1 = pesel.peselProperty().get();
        System.out.println(nick1 + ", " + nr1);
    }
}
