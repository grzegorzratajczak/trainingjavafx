package bookJavaFX9.chapter01;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.util.List;
import java.util.Map;

/**
 * application for taking parameters in launch
 */
public class Listing01_7 extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parameters parameters = this.getParameters();
        Map<String, String> nParams = parameters.getNamed();
        List<String> unParams = parameters.getUnnamed();
        List<String> rParams = parameters.getRaw();
        TextArea textArea = new TextArea(nParams + "\n" + unParams + "\n" + rParams);
        StackPane root = new StackPane();
        root.getChildren().add(textArea);
        Scene scene = new Scene(root, 600, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
