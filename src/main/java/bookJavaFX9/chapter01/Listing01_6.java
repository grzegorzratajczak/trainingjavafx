package bookJavaFX9.chapter01;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Example of simple label, button, and static method Platform.exit()
 */
public class Listing01_6 extends Application {

    public void start(Stage primaryStage) throws Exception {
        System.out.println("Launch start()method");
        Label label = new Label("Hello world");
        VBox root = new VBox();
        Button stopButton = new Button("Goodbye world");
        stopButton.setOnAction(e -> {
            System.out.println("stopButton click");
            Platform.exit();
        });
        root.getChildren().addAll(label, stopButton);
        Scene scene = new Scene(root, 600, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
