package bookJavaFX9.chapter01;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * example of create simple window
 */
public class Listing01_4 extends Application {

    public void start(Stage primaryStage) throws Exception {

        try{
            StackPane root = new StackPane();
            Scene scene = new Scene(root, 300,200);
            primaryStage.setScene(scene);
            primaryStage.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
