package bookJavaFX9.chapter02;

/**
 * main for example of JavaBean with listener
 */
public class Listing02_02Main {

    public static void main(String[] args) {
        Listing02_02 dragon = new Listing02_02();
        dragon.addPropertyChangeListener(evt -> {
            Integer newValue = (Integer) evt.getNewValue();
            System.out.println(newValue);
        });
        for(int i=0; i <11; i++){
            dragon.setCalNumber(i * 1000);
        }
    }
}
