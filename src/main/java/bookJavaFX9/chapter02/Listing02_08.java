package bookJavaFX9.chapter02;

import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;

/**
 * JavaFXBean Read only properties
 */
public class Listing02_08 {

    private final ReadOnlyStringWrapper nick = new ReadOnlyStringWrapper(this, "nick", "Unknownn");
    private final ReadOnlyIntegerWrapper pesel = new ReadOnlyIntegerWrapper(this, "pesel");

    public Listing02_08(String nick, int pesel) {
        this.nick.set(nick);
        this.pesel.set(pesel);
    }

    public final String getNick(){
        return nick.get();
    }

    public final ReadOnlyStringProperty nickProperty(){
        return nick.getReadOnlyProperty();
    }

    public final int getPesel(){
        return pesel.get();
    }

    public final ReadOnlyIntegerProperty peselProperty(){
        return pesel.getReadOnlyProperty();
    }
}
