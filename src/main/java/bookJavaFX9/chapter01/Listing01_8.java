package bookJavaFX9.chapter01;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.util.List;

/**
 * example of set window width and height from parameters
 */
//will crash when try to launch without 2 unnamed parameters
public class Listing01_8 extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parameters parameters = this.getParameters();
        List<String> unParameters = parameters.getUnnamed();
        StackPane root = new StackPane();
        Scene scene = new Scene(root, Double.parseDouble(unParameters.get(0)), Double.parseDouble(unParameters.get(1)));
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
